package com.rick.ubc_test;

import java.io.File;

public class Main
{
	//////////
    // JNI interfaces to ubc32.dll or ubc64.dll
	// BEGIN
	//////
		//
		//////
			static
			{
				if (System.getProperty("sun.arch.data.model").equals("32"))
				{
					// 32-bit JVM
					System.loadLibrary("ubc32");

				} else {
					// 64-bit JVM
					System.loadLibrary("ubc64");
				}
			}

			// Create a new assembly
			public native static long/*uid_root*/				ubc_create				();
			
			// Reference and debug info functions
			public native static int/*iid_file*/				ubc_queueFile			(String pathname, boolean loadCopyIntoUbc, boolean storeExternal);
			public native static int/*iid_ref*/					ubc_queueReference		(long offset, long length, int bit_offset, int bit_length, boolean isBigEndian, boolean isDebugInfo, boolean isType, boolean isFunction, boolean isParameter, String referenceName, int iid_relativeTo, int iid_type, int iid_file, long line_number, long line_offset);

			// Add content to the assembly
			public native static long/*uid_data_blocks*/		ubc_addDataBlocks		(long uid_root, long uid_data_blocks[]);
			public native static long/*uid_code_blocks*/		ubc_addCodeBlocks		(long uid_root, long uid_code_blocks[]);
			public native static long/*uid_code or uid_data*/	ubc_addCodeOrDataBlock	(long uid_root, long uid_ref, long uid_debug, long uid_cond, boolean execute, boolean write, boolean read, String data, int granularity, int page_count, int init_byte);
			public native static long/*uid_build_date*/			ubc_addBuildDate		(long uid_root, String date8, String time9, int iteration, long uid_isa_os);
			public native static long/*uid_isa_os*/				ubc_addIsaOs			(long uid_root, String isa, String os, long uid_data_block, long uid_code_block);
			public native static long/*uid_data*/				ubc_addData				(long uid_root, String code);
			public native static long/*uid_code*/				ubc_addCode				(long uid_root, String code);
			public native static long/*uid_build_ref*/			ubc_addReference		(long uid_root, long uid_code_or_data, int iid_blocks[]);

			// Publish and run the assembly
			public native static long/*success*/				ubc_publish				(long uid_root, String pathname);
			public native static long/*success*/				ubc_launch				(String appName, String pathName, String cmdLine);
		//////
		//
	//////
	// END
    // JNI interface to ubc32.dll or ubc64.dll
	//////////
	
	public static void main(String[] args)
	{
		long		asm;
		String		binCode, binData, lcFile;

		
		// Create a new assembly
		asm = ubc_create();
		
		// Add ISA and OS information
		ubc_addIsaOs(asm, "x64", "Win64", 0, 0);
		
		// Add some code
		binCode = "Code Here";
		ubc_addData(asm, binCode);
		
		// Add some data
		binData = "Data Here";
		ubc_addCode(asm, binData);
		
		// Publish the assembly to a disk file
		lcFile = "test.exe";
		ubc_publish(asm, lcFile);
		
		// Launch the executable
		System.out.println("Result code = " + Long.toString(ubc_launch("Test App", lcFile, lcFile + " p1 p2 p3")));
		System.exit(0);
	}
}
