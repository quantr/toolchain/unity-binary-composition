//////////
//
// structs.h
// Structure definitions
//
//////
//
// UBC -- Unity Binary Composition
// Apr.19.2020
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
//
#pragma once

// Required forward declarations
struct SUbcLink;




// Each of these structures must be byte-aligned
#pragma pack(push, 1)
//{

	//////////
	// Standard structures
	//////
		struct SUbcHdr
		{
			u16		type;           // 0, 2		See _TYPE_* constants, all types 1000 and above are available for custom use
			u64		uid;			// 2, 8		Unique ID for this name, always positive, but signed for references
			u64	    length;         // 10, 8	Length of the entire packet
			// Total:  18 bytes
			// Note:  Variable-length data appears after this
		};

		// Appears at the head of the UBC block signifying the start of the two bookends, SUbcSig1 on left, SUbcSig2 on right
		struct SUbcSig1
		{
			u64		magic_number;		// 0x1223334444555556
			u32		ubc_length;			// The length of the UBC block between SUbcSig1 and SUbcSig2
		};

		// Appears at end of launcher.exe
		struct SUbcSig2
		{
			u64		magic_number;		// 0x7777777888888889
			u32		ubc_length;			// The length of the UBC block between SUbcSig1 and SUbcSig2
		};

		struct SUbcText
		{
			u16		length;				// How long is the text
			w16		text[1];			// Holds length w16s
		};

		struct SUbcError
		{
			u32			errorCode;		// The error code, see _UBC_ERROR_* constants
			wchar_t*	message;		// A custom message related to whatever the error was
		};




	//////////
	// Version
	//////
		struct SUbcVer
		{
			// Standard header
			SUbcHdr			header;

			// Flags holds what additional information is included:
			struct flags
			{
				s8		nuserved		: 6;
				bool	hasIsaOs		: 1;		// Is the uid_isa_os valid?
				bool	hasExtraVersion	: 1;		// Is the extra_version member valid?
				// Total:  8 bits
			};

			// One version string is always included
			SUbcText		ubc_version;
			SUbcText		isa_os_version;

			// Options available based on flags
			u64				uid_isa_os;
			SUbcText		extra_version;
		};




	//////////
	// The ISA/OS settings
	//////
		struct SBitsIsaOs
		{
			// Divided into available and required parts
			u32				available	: 24;		// Upper 24-bits are not used, and are reserved for ISA- or OS-specific use
			u8				type		: 8;		// Lower 8-bits are used to identify the ISA or OS
		};

		struct SUbcIsaOsBlocks
		{
			// Will be one of the _TYPE_* constants
			u8				type;

			// List of block uids that are to be used for this compilation
			u16				block_count;
			u64				block_array[1];
		};

		struct SUbcIsaOs
		{
			// Standard header
			SUbcHdr			header;

			// Constants used for the ISA and OS
			SBitsIsaOs		isa;		// See _ISA_* constants
			SBitsIsaOs		os;			// See _OS_* constants

			// Optional information may be present
			struct
			{
				s8			nuserved			: 4;
				bool		hasDataBlockList	: 1;	// Is uid_data_block member valid?
				bool		hasCodeBlockList	: 1;	// Is uid_code_block member valid?
				bool		hasIsaVersion		: 1;	// Is isa_info member present?
				bool		hasOsVersion		: 1;	// Is os_info member present?
				// Note:  If it doesn't have blocks lists, it's assumed that every block within is for this build
				// Total:  8 bits
			} flags;

			// UBC can contain multiple targets within a single binary.  This holds the
			// uid for the list of blocks associated with this ISA/OS target.
			u64				uid_data_block;				// (SUbcIsaOsBlocks)
			u64				uid_code_block;				// (SUbcIsaOsBlocks)

			// Text for these options (if they're present)
			//SUbcText		isa_version;
			//SUbcText		os_version;
		};




	//////////
	// The Build Date
	//////
		struct SUbcBd
		{
			// Standard header
			SUbcHdr			header;

			// The build date
			w16				date8[8];				// YYYYMMDD or Julian Day Number if leading 0
			w16				time9[9];				// HHMMSSmmm
			u32				iteration;				// For build histories

			u64				uid_isa_os;				// The IsaOs this relates to
		};




	//////////
	// The _DATA and _CODE structures
	//////
		struct SUbcCode
		{
			u64				uid_code;				// The _CODE block this relates to

			// If this code has main() or _init(), offsets to access them.  -1 if not populated.
			u64				offset_main;			// Offset into the _CODE block for main()
			u64				offset__init;			// Offset into the _CODE block for _init(), called before main()
		};

		struct SUbcDcFlags
		{
			u32				nuserved		: 32;

			// Are these members valid?
			bool			hasUidRef		: 1;	// Is uid_ref valid?
			bool			hasUidDebug		: 1;	// Is uid_debug valid?
			bool			hasUidCond		: 1;	// Is uid_cond valid?

			// Data real or manufactured?
			bool			dataIncluded	: 1;	// 0=yes, 1=must allocate

			// For allocation
			bool			granularity		: 1;	// 0=4KB pages, 1=4MB pages
			s32				page_count		: 16;	// Number of pages to allocates
			s8				init_byte		: 8;	// Initialize character for memory

			// Data or Code block flags
			bool			execute			: 1;	// Only for _CODE blocks
			bool			write			: 1;	// Writable
			bool			read			: 1;	// Readable
		};

		struct SUbcDc
		{
			// Standard header
			SUbcHdr			header;

			// Type of data or code block
			SUbcDcFlags		flags;

			// Optional information may be present
			u64				uid_reference;		// _TYPE_REFERENCES
			u64				uid_debug;			// _TYPE_DEBUG
			u64				uid_cond;			// _TYPE_CONDITIONALS

			// The remainder of content is the code or data block (meaning (header->length - sizeof(SUbcDc)) bytes)
			//s8			code_or_data[1]
		};




	//////////
	// References for fixups, as well as debugging symbols, source code, filenames
	//////
		//{
			struct SUbcRefLineAndOffset
			{
				u16		iid_file;			// The file iid this line and offset relate to (iid with isFileRef=1)
				u32		line;				// Line number within the file
				u32		offset;				// 
			};

			struct SUbcRefFlags
			{
				u16		iid					: 16;		// Unique ID within this array for a symbol
				bool	isFileRef			: 1;		// 1-has filename, but for reference, 0-no
				bool	external			: 1;		// Is this information stored in an external file?
				u8		nuserved			: 5;

				// If external=0, these values are populated with valid data
				bool	isDebug				: 1;		// 1-debug info, 1-reference
				bool	isBigEndian			: 1;		// 1-big, 0-little
				bool	isType				: 1;		// 1-type, 0-reference
				bool	isFunction			: 1;		// 1-function, 0-variable
				bool	isParameter			: 1;		// 1-parameter, 0-global or local depending on what it's relative to

				// For debugging
				bool	hasSymbolName		: 1;		// 1-symbol, 0-no symbol
				bool	hasLineAndOffset	: 1;		// 1-line + offset, 0-none
				bool	hasRelativeTo		: 1;		// Is this relative to another entry?  iid_relativeTo is valid?

				// If it has a filename or symbol name, the length here will be populated
				union
				{
					u16 filename_length		: 9;		// external = 1
					u16	symbolname_length	: 9;		// external = 0 and isReference = 1 and hasSymbol = 1
					u16 typename_length		: 9;		// external = 0 and isReference = 0 and hasType = 1
				};

				u16		iid_relativeTo		: 16;		// If part of a sub-structure, the parent structure
				u16		iid_type			: 16;		// if not -1, the type definition this relates to
				// Total:  64-bits

				// If present, filename, symbol name, or type will appear after
				// SUbcText name

				// If hasLineAndOffset, this struct will appear after
				// SUbcRefLineAndOffset
			};

			struct SUbcRef
			{
				// It's either a fixup or a debug reference
				SUbcRefFlags	flags;

				u64				offset;					// Offset into the encapsulating SUbcDc
				u32				length;					// Length of the thing

				// If it's a bit struct
				u8				bit_offset;				// Starting bit into the offset and length (relative to endian)
				u8				bit_length;				// Number of bits there
			};
		//}

		struct SUbcRefs
		{
			// Standard header
			SUbcHdr			header;

			// Reference array
			u64				uid_relatedTo;				// uid of the code or data block this relates to
			u16				count;
			SUbcRef			items[1];
		};




	//////////
	// Conditionals
	//////
		struct SUbcCond
		{
			// Standard header
			SUbcHdr			header;

			// Functions (if present)
			u32				count_loadBlocks;
			u32				count_positionBlocks;

			// Sequential arrays for each one
			// SUbcCode		loadBlocks[];				// bool (*func_loadBlock)		(u32 uid_blockNum)
			// SUbcCode		positionBlocks[];			// bool (*func_positionBlock)	(u32 uid_blockNum)
		};

//}
#pragma pack(pop)


//////////
// Link structure
//////
	struct SUbcStats
	{
		// Total size (in bytes) and number of elements
		s32				size;
		s32				elements;

		// Pointers to the first and last items in the chain
		SUbcLink*		first;
		SUbcLink*		last;
	};

	struct SUbcLink
	{
		// Two-way link-list
		SUbcLink*		next;
		SUbcLink*		prev;

		// Discriminating union
		s32				type;
		union
		{
			void*			vptr;
			SUbcStats*		stats;
			SUbcHdr*		hdr;
			SUbcSig1*		sig1;
			SUbcSig2*		sig2;
			SUbcVer*		version;
			SUbcIsaOs*		isa_os_bits;
			SUbcBd*			build_date;
			SUbcDc*			data_code;
			SUbcRefs*		references;
			SUbcCond*		conditionals;
		};
	};
