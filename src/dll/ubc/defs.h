//////////
//
// defs.h
// Function prototype definitions
//
//////
//
// UBC -- Unity Binary Composition
// Apr.19.2020
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
//
#pragma once




//////////
// ubc_jni.h
//////
	// Function targets called from java.exe
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1create				(JNIEnv* env, jclass cls);
	JNIEXPORT jint JNICALL		Java_com_rick_ubc_1test_Main_ubc_1queueFile				(JNIEnv* env, jclass cls, jstring pathname, jboolean loadCopyIntoUbc, jboolean storeExternal);
	JNIEXPORT jint JNICALL		Java_com_rick_ubc_1test_Main_ubc_1queueReference		(JNIEnv* env, jclass cls, jlong offset, jlong length, jint big_offset, jint bit_length, jboolean isBigEndian, jboolean isDebugInfo, jboolean isType, jboolean isFunction, jboolean isParameter, jstring referenceName, jint iid_relativeTo, jint iid_type, jint iid_file, jlong line_number, jlong line_offset);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addDataBlocks			(JNIEnv* env, jclass cls, jlong uid_root, jlongArray dataBlocks);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addCodeBlocks			(JNIEnv* env, jclass cls, jlong uid_root, jlongArray codeBlocks);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addCodeOrDataBlock	(JNIEnv* env, jclass cls, jlong uid_root, jlong uid_ref, jlong uid_debug, jlong uid_cond, jboolean execute, jboolean write, jboolean read, jstring data, jint granularity, jint page_count, jint init_byte);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addBuildDate			(JNIEnv* env, jclass cls, jlong uid_root, jstring date8, jstring time9, jint iteration, jlong uid_isa_os);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addIsaOs				(JNIEnv* env, jclass cls, jlong uid_root, jstring isa, jstring os, jlong uid_data_block, jlong uid_code_block);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addData				(JNIEnv* env, jclass cls, jlong uid_root, jstring data);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addCode				(JNIEnv* env, jclass cls, jlong uid_root, jstring code);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1addReference			(JNIEnv* env, jclass cls, jlong uid_root, jlong uid_code_or_data, jintArray iid_blocks);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1publish				(JNIEnv* env, jclass cls, jlong uid_root, jstring pathname);
	JNIEXPORT jlong JNICALL		Java_com_rick_ubc_1test_Main_ubc_1launch				(JNIEnv* env, jclass cls, jstring appname, jstring pathname, jstring cmdLine, jstring directory);


//////////
// ubc_public.h
//////
	// Public API function prototypes
	#include "ubc.h"




//////////
// ubc_misc.h
//////
	int							iUbc_openFile									(const w16* pathname, s64* tnFileSize = NULL);
	bool						iUbc_readSignatures								(int fileno, SUbcSig1* sig1, SUbcSig2* sig2, s64* ubc_offset);
	bool						iUbc_doesFileExist								(w16* pathname);

	// Allocate/deallocate functions
	SUbcLink*					iiUbc_new_rootLink								(SUbcError** error = NULL);
	SUbcLink*					iiUbc_updateLinkList							(SUbcLink* ubcRoot, SUbcLink* ubcNew, SUbcError** error = NULL);
	SUbcLink*					iiUbc_newLink_bySizeAndType						(SUbcLink* ubcRoot, s32 blockSize, s32 type, SUbcError** error = NULL);
	SUbcBd*						iUbc_add_build_date								(SUbcLink* ubcRoot, w16* jniDate8, w16* jniTime9, jint iteration, jlong uid_isa_os, SUbcError** error = NULL);
	SUbcIsaOs*					iUbc_add_isa_os									(SUbcLink* ubcRoot, w16* jniIsa, w16* jniOs, jlong uid_data_block, jlong uid_code_block, SUbcError** error = NULL);
	s32							iiUbc_calcSbcTextSize							(w16* text, SUbcText** textOut = NULL, SUbcError** error = NULL);
	SUbcError*					iiUbc_addError									(u32 errorCode, w16* message);
	w16*						iiUbc_getFirstNnChars							(w16* text, s32 length);