//////////
//
// ubc_jni.h
//
//////
//
// Unity Binary Composition JNI support.
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
// Refer to:  https://www.codeproject.com/Articles/993067/Calling-Java-from-Cplusplus-with-JNI
// Refer to:  https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html
// Ability to create a JVM in a C++ app, and then run code in it, including calling its Java
// functions from C++.
//
//////
//
//		C++ Type				Size					Java Language Type		Common Windows Types
//	 ---------------------	--------------------	------------------		------------------------
//		char					8-bit integer			byte					BYTE, TCHAR
//		short					16-bit	short			short					WORD
//		wchar_t					16/32-bit character		char					WCHAR, TCHAR
//		int						32-bit integer			int						DWORD
//		int						boolean value			boolean					BOOL
//		long					32/64-bit integer		NativeLong				LONG
//		long long, __int64		64-bit integer			long					QWORD
//		float					32-bit FP				float
//		double					64-bit FP				double
//		char*					C string				String					LPTCSTR
//		void*					pointer					Pointer					LPVOID, HANDLE, LPXXX
//
//////////
//
//			Type Signature					Java Type
//		-----------------------------	-----------------------
//			Z							boolean
//			B							byte
//			C							char
//			S							short
//			I							int
//			J							long
//			F							float
//			D							double
//			L fully-qualified-class ;	fully-qualified-class
//			[ type						type[]
//			( arg-types ) ret-type		method type
//
//////////
//
//		Java Type		JNI Type		Data Size
//		---------		--------		----------------
//		boolean			jboolean		unsigned 8 bits
//		char			jchar			unsigned 16 bits
//
//		byte			jbyte			signed 8 bits
//		short			jshort			signed 16 bits
//		int				jint			signed 32 bits
//		long			jlong			signed 64 bits
//
//		float			jfloat			32 bits
//		double			jdouble			64 bits
//
//		void			void			N/A
//
//////
#pragma once




//////////
//
//	public native static int/*uid_root*/			ubc_create				();
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1create(JNIEnv* env, jclass cls)
	{
		union
		{
			jint			_id;
			SUbcLink*		ubcRoot;
		};


		// Allocate a new root item
		ubcRoot = iiUbc_new_rootLink();

		// Signify
		return _id;
	}




//////////
//
//	public native static int/*iid_file*/				ubc_queueFile			(String pathname, boolean loadCopyIntoUbc, boolean storeExternal);
//
//////
	JNIEXPORT jint JNICALL Java_com_rick_ubc_1test_Main_ubc_1queueFile(JNIEnv* env, jclass cls, jstring pathname, jboolean loadCopyIntoUbc, jboolean storeExternal)
	{
		return -1;
	}




//////////
//
//	public native static int/*iid_ref*/					ubc_queueReference		(long offset, long length, int bit_offset, int bit_length, boolean isBigEndian, boolean isDebugInfo, boolean isType, boolean isFunction, boolean isParameter, String referenceName, int iid_relativeTo, int iid_type, int iid_file, long line_number, long line_offset);
//
//////
	JNIEXPORT jint JNICALL Java_com_rick_ubc_1test_Main_ubc_1queueReference(JNIEnv* env, jclass cls, jlong offset, jlong length, jint big_offset, jint bit_length, jboolean isBigEndian, jboolean isDebugInfo, jboolean isType, jboolean isFunction, jboolean isParameter, jstring referenceName, jint iid_relativeTo, jint iid_type, jint iid_file, jlong line_number, jlong line_offset)
	{
		return -1;
	}




//////////
//
//	public native static int/*uid_data_blocks*/		ubc_addDataBlocks		(int uid_root, int uid_data_blocks[]);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addDataBlocks(JNIEnv* env, jclass cls, jlong uid_root, jlongArray dataBlocks)
	{
		jsize	count;
		jlong*	dataArray;


		// Grab the array content
		dataArray	= env->GetLongArrayElements(dataBlocks, false);
		count		= env->GetArrayLength(dataBlocks);

		// Append these blocks
// TODO:  code here

		// All done
		env->ReleaseLongArrayElements(dataBlocks, dataArray, 0);
		return count;
	}




//////////
//
//	public native static int/*uid_code_blocks*/		ubc_addCodeBlocks		(int uid_root, int uid_code_blocks[]);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addCodeBlocks(JNIEnv* env, jclass cls, jlong uid_root, jlongArray codeBlocks)
	{
		jsize	count;
		jlong*	codeArray;


		// Grab the array content
		codeArray	= env->GetLongArrayElements(codeBlocks, false);
		count		= env->GetArrayLength(codeBlocks);

		// Append these blocks
// TODO:  code here

		// All done
		env->ReleaseLongArrayElements(codeBlocks, codeArray, 0);
		return count;
	}




//////////
//
// public native static int/*uid_code or uid_data*/	ubc_addCodeOrDataBlock	(int uid_root, int uid_ref, int uid_debug, int uid_cond, boolean execute, boolean write, boolean read, String data, int granularity, int page_count, int init_byte);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addCodeOrDataBlock(JNIEnv* env, jclass cls, jlong uid_root, jlong uid_ref, jlong uid_debug, jlong uid_cond, jboolean execute, jboolean write, jboolean read, jstring data, jint granularity, jint page_count, jint init_byte)
	{
		return -1;
	}




//////////
//
// public native static int/*uid_build_date*/		ubc_addBuildDate		(int uid_root, String date8, String time9);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addBuildDate(JNIEnv* env, jclass cls, jlong uid_root, jstring date8, jstring time9, jint iteration, jlong uid_isa_os)
	{
		const jchar*	jniDate8;
		const jchar*	jniTime9;

		union
		{
			jlong		_uid_root;
			SUbcLink*	ubcRoot;
		};

		union
		{
			jlong 		lnResult;
			SUbcBd*		bd;
		};


		// Grab our structure
		_uid_root = uid_root;

		// Grab the data
		jniDate8	= env->GetStringChars(date8, false);
		jniTime9	= env->GetStringChars(time9, false);

		// Add the Date and time information
		bd			= iUbc_add_build_date(ubcRoot, (w16*)jniDate8, (w16*)jniTime9, iteration, uid_isa_os);

		// Release
		if (jniDate8)		env->ReleaseStringChars(date8, jniDate8);
		if (jniTime9)		env->ReleaseStringChars(time9, jniTime9);

		// Signify
		return lnResult;

	}




//////////
//
//	public native static int/*uid_isa_os*/			ubc_addIsaOs			(int uid_root, String isa, String os, int uid_data_block, int uid_code_block);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addIsaOs(JNIEnv* env, jclass, jlong uid_root, jstring isa, jstring os, jlong uid_data_block, jlong uid_code_block)
	{
		const jchar*		jniIsa;
		const jchar*		jniOs;

		union
		{
			jlong			_uid_root;
			SUbcLink*		ubcRoot;
		};

		union
		{
			jlong			lnResult;
			SUbcIsaOs*		isaOs;
		};


		// Grab our structure
		_uid_root = uid_root;

		// Grab the data
		jniIsa	= env->GetStringChars(isa,	false);
		jniOs	= env->GetStringChars(os,	false);

		// Add the ISA and OS information
		isaOs	= iUbc_add_isa_os(ubcRoot, (w16*)jniIsa, (w16*)jniOs, uid_data_block, uid_code_block);

		// Release
		if (jniIsa)		env->ReleaseStringChars(isa, jniIsa);
		if (jniOs)		env->ReleaseStringChars(os,  jniOs);

		// Signify
		return lnResult;
	}




//////////
//
//	public native static int/*uid_data*/			ubc_addData				(int uid_root, String code);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addData(JNIEnv* env, jclass cls, jlong uid_root, jstring data)
	{
		return 3;
	}




//////////
//
//	public native static int/*uid_code*/			ubc_addCode				(int uid_root, String code);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addCode(JNIEnv* env, jclass cls, jlong uid_root, jstring code)
	{
		return 4;
	}




//////////
//
//	public native static long/*uid_build_ref*/			ubc_addReference		(long uid_root, long uid_code_or_data, int iid_blocks[]);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1addReference(JNIEnv* env, jclass cls, jlong uid_root, jlong uid_code_or_data, jintArray iid_blocks)
	{
		return 5;
	}




//////////
//
// public native static int/*success*/		ubc_publish			(int id, String pathname);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1publish(JNIEnv* env, jclass cls, jlong uid_root, jstring pathname)
	{
		return 6;
	}




//////////
//
// public native static int/*success*/		ubc_launch			(String pathname);
//
//////
	JNIEXPORT jlong JNICALL Java_com_rick_ubc_1test_Main_ubc_1launch(JNIEnv* env, jclass cls, jstring appname, jstring pathname, jstring cmdLine, jstring directory)
	{
		long				lnResult;
		const jchar*		jniAppName;
		const jchar*		jniPathname;
		const jchar*		jniCmdLine;
		const jchar*		jniDirectory;


		//////////
		// Make sure our environment is sane
		//////
			jniAppName		= env->GetStringChars(appname,		false);
			jniPathname		= env->GetStringChars(pathname,		false);
			jniCmdLine		= env->GetStringChars(cmdLine,		false);
			jniDirectory	= env->GetStringChars(directory,	false);
			if (pathname && jniPathname && env->GetStringLength(pathname) > 0)
			{
				// If we don't have a command line, then use the pathname
				if (!jniCmdLine)
					jniCmdLine = jniPathname;

				// Try to launch
				lnResult = ubc_launch_exe(NULL, (w16*)jniPathname, (w16*)jniCmdLine, (w16*)directory);

			} else {
				// Invalid
				lnResult = _ERROR_INVALID_PARAMETERS;
			}


		//////////
		// Release
		//////
			if (jniCmdLine && jniCmdLine != jniPathname)		env->ReleaseStringChars(cmdLine, jniCmdLine);
			if (jniAppName)										env->ReleaseStringChars(appname, jniAppName);
			if (jniPathname)									env->ReleaseStringChars(pathname, jniPathname);
			if (jniDirectory)									env->ReleaseStringChars(directory, jniDirectory);


		//////////
		// Signify
		//////
			return lnResult;

	}
