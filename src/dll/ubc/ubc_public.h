//////////
//
// ubc_public.h
//
//////
//
// Unity Binary Composition
// A cross-platform / cross-OS ABI for linked libraries and executables.
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
// Refer to:  https://www.codeproject.com/Articles/993067/Calling-Java-from-Cplusplus-with-JNI
// Refer to:  https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html
// Ability to create a JVM in a C++ app, and then run code in it, including calling its Java
// functions from C++.
//
//////
#pragma once




//////////
//
// Called to launch from the current EXE that's running right now.  Used to locate
// and load the UBC ABI, construct it into memory, and transfer control there.
//
//////
	int ubc_launch(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
	{
		s64			lnUbcOffset;
		int			fileno;
		s8*			exe;
		wchar_t		szFileName[MAX_PATH];
		wchar_t		buffer[_MAX_PATH * 2];
		SUbcSig1	sig1;
		SUbcSig2	sig2;


		// Grab the file name
		GetModuleFileNameW(NULL, szFileName, MAX_PATH);

		// Open the file
		fileno = iUbc_openFile(szFileName);
		if (fileno >= 0 && iUbc_readSignatures(fileno, &sig1, &sig2, &lnUbcOffset))
		{
			// Allocate load content
			if ((exe = (s8*)malloc(sig1.ubc_length)))
			{
				// Read the content
				_read(fileno, exe, sig1.ubc_length);

				// If we get here, not found
				MessageBox(NULL, L"UBC is valid.", L"Success", MB_OK);
				return 0;

			} else {
				// If we get here, not found
				MessageBox(NULL, L"Cannot allocate memory.", L"Failure", MB_OK);
				return 0;
			}
		}

		// If we get here, not found
		swprintf_s(buffer, sizeof(buffer), L"Unable to open %s for reading\n", szFileName);
		MessageBox(NULL, buffer, L"Failure", MB_OK);
		return 0;
	}




//////////
//
// Launches the EXE pathname as a new process.
//
// If launching "hello.exe" should send in:
//
//		appName			= "Hello"
//		exePathname		= "hello.exe"
//		cmdLine			= "hello.exe params go here"
//		directory		= "c:\path\to\start\location\"
//
//////
	int ubc_launch_exe(wchar_t* appName, wchar_t* exePathname, wchar_t* cmdLine, wchar_t* directory)
	{
		wchar_t		currentDirectory[_MAX_PATH];


		// Make sure the file exists
		if (!iUbc_doesFileExist(exePathname))
			return _ERROR_FILE_DOES_NOT_EXIST;

		// Create a process and launch it
		if (!directory)
		{
			GetCurrentDirectory(sizeof(currentDirectory), currentDirectory);
			directory = currentDirectory;
		}
		return (CreateProcess(appName, cmdLine, NULL, NULL, FALSE, 0, NULL, directory, NULL, NULL) != 0);
	}
