//////////
//
// defs.h
// Function prototype definitions
//
//////
//
// UBC -- Unity Binary Composition
// Apr.19.2020
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
//
#pragma once




//////////
//
// Called to open the indicated file, and optionally set the file length
//
//////
	int iUbc_openFile(const w16* pathname, s64* tnFileSize)
	{
		int fileno;


		// Grab
		fileno = 0;
		_wsopen_s(&fileno, pathname, _O_BINARY | _O_RDONLY, _SH_DENYNO, _S_IREAD);
		if (fileno >= 0)
		{
			// Get the file size (if requested)
			if (tnFileSize)
			{
				_lseeki64(fileno, 0, SEEK_END);
				*tnFileSize = _telli64(fileno);
				_lseeki64(fileno, 0, SEEK_SET);
			}
		}

		// Signify
		return fileno;
	}




//////////
//
// Reads sig1 and sig2 from the payload attached to the launcher (if present)
//
//////
	bool iUbc_readSignatures(int fileno, SUbcSig1* sig1, SUbcSig2* sig2, s64* ubc_offset)
	{
		int			numread;
		s64			lnPrevOffset, lnFileSize;
		SUbcSig1	_sig1;
		SUbcSig2	_sig2;


		// Make sure we have signatures
		if (!sig1)	sig1 = &_sig1;
		if (!sig2)	sig2 = &_sig2;

		// Save original offset
		lnPrevOffset = _telli64(fileno);

		// Seek to the end and get file size
		_lseeki64(fileno, 0, SEEK_END);
		lnFileSize = _telli64(fileno);

		// Seek to sig2, read, and seek back
		_lseeki64(fileno, -(s64)sizeof(*sig2), SEEK_END);
		numread = _read(fileno, sig2, sizeof(*sig2));
		_lseeki64(fileno, lnPrevOffset, SEEK_SET);

		// Did it read okay and is it valid?
		if (numread == sizeof(*sig2) && sig2->magic_number == _SIG2_MAGIC_NUMBER)
		{
			// Signature 2 is valid
			// Seek to sig1, read, and seek back
			_lseeki64(fileno, -(s64)(sizeof(*sig2) + sizeof(*sig1) + sig2->ubc_length), SEEK_END);
			numread = _read(fileno, sig1, sizeof(*sig1));
			_lseeki64(fileno, lnPrevOffset, SEEK_SET);

			// Did it read okay and is it valid?
			if (numread == sizeof(*sig1) && sig1->magic_number == _SIG1_MAGIC_NUMBER && sig1->ubc_length == sig2->ubc_length)
			{
				// Signature 1 is valid
				// Store where the UBC begins (if needed)
				if (ubc_offset)
					*ubc_offset = lnFileSize - sizeof(*sig2) - sig2->ubc_length;

				// Signify success
				return true;
			}
		}

		// If we get here, invalid
		return false;
	}




//////////
//
// Called to see if the indicated file exists
//
//////
	bool iUbc_doesFileExist(w16* pathname)
	{
		WIN32_FIND_DATA		fd;
		HANDLE				h;


		// Try to find the first file
		h = FindFirstFile(pathname, &fd);
		if (h != INVALID_HANDLE_VALUE)
		{
			CloseHandle(h);
			return true;
		}

		// If we get here, not found
		return false;
	}




//////////
//
// Called to allocate a new root UBC Link
//
//////
	SUbcLink* iiUbc_new_rootLink(SUbcError** error)
	{
		SUbcLink*	ubc;


		// Allocate and initialize a new one
		if ((ubc = (SUbcLink*)calloc(1, sizeof(*ubc))))
		{
			// Add a statistics item
			ubc->type	= _UBC_LINK_TYPE_STATS;
			ubc->stats	= (SUbcStats*)calloc(1, sizeof(SUbcStats));
			// The stats item is always first, and it doesn't count in the size or elements
		}

		// Signify
		return ubc;
	}




//////////
//
// Called to add a new item to the link list
//
//////
	SUbcLink* iiUbc_updateLinkList(SUbcLink* ubcRoot, SUbcLink* ubcNew, SUbcError** error)
	{
		SUbcLink* ubcOldLast;


		// Are we on a chain?  Or just starting one?
		if (ubcRoot->stats->first)
		{
			// There is at least one item in the chain
			ubcOldLast				= ubcRoot->stats->last;

			// Append it to the end
			ubcOldLast->next		= ubcNew;				// Old last points forward to new
			ubcNew->prev			= ubcOldLast;			// New points back to old last
			ubcRoot->stats->last	= ubcNew;				// New becomes last

		} else {
			// This is the first item
			ubcOldLast				= NULL;					// No prior last item
			ubcRoot->stats->first	= ubcNew;				// First points to new
			ubcRoot->stats->last	= ubcNew;				// Last points to new
		}

		// Signify the old last
		return ubcOldLast;
	}




//////////
//
// Called to allocate a new UBC Link
//
//////
	SUbcLink* iiUbc_newLink_bySizeAndType(SUbcLink* ubcRoot, s32 blockSize, s32 type, SUbcError** error)
	{
		SUbcLink* ubc;


		// Allocate and initialize a new one
		if ((ubc = (SUbcLink*)calloc(1, sizeof(*ubc))))
		{
			// Add a statistics item
			ubc->type	= type;
			ubc->vptr	= (SUbcStats*)calloc(1, blockSize);

			// Update the link lists
			iiUbc_updateLinkList(ubcRoot, ubc);
		}

		// Signify
		return ubc;
	}




//////////
//
// Called to add the build date and time
//
//////
	SUbcBd* iUbc_add_build_date(SUbcLink* ubcRoot, w16* jniDate8, w16* jniTime9, jint iteration, jlong uid_isa_os, SUbcError** error)
	{
		SUbcBd*		bd;


		// Create a new record
		if ((bd = (SUbcBd*)iiUbc_newLink_bySizeAndType(ubcRoot, sizeof(SUbcBd), _UBC_LINK_TYPE_BUILD_DATE)))
		{
			// Populate
			wcscpy_s(bd->date8, (sizeof(bd->date8) / sizeof(w16)), jniDate8);
			wcscpy_s(bd->time9, (sizeof(bd->time9) / sizeof(w16)), jniTime9);

			bd->iteration	= iteration;
			bd->uid_isa_os	= uid_isa_os;
		}

		// Signify
		return bd;

	}




//////////
//
// Called to add a new ISA/OS entry.
// Returns the element number for the added item.
//
//////
	SUbcIsaOs* iUbc_add_isa_os(SUbcLink* ubcRoot, w16* jniIsa, w16* jniOs, jlong uid_data_block, jlong uid_code_block, SUbcError** error)
	{
		s32			lnLength, lnLength_isa, lnLength_os;
		SUbcIsaOs*	io;
		SUbcText*	isa;
		SUbcText*	os;


		// Calculate the length
		isa				= NULL;
		os				= NULL;
		lnLength_isa	= iiUbc_calcSbcTextSize(jniIsa, &isa);
		lnLength_os		= iiUbc_calcSbcTextSize(jniOs, &os);
		lnLength		= sizeof(SUbcIsaOs) + lnLength_isa + lnLength_os;

		// Create a new record
		io = (SUbcIsaOs*)iiUbc_newLink_bySizeAndType(ubcRoot, lnLength, _UBC_LINK_TYPE_ISA_OS);
		if (io)
		{

			//////////
			// Populate
			//////
				io->flags.hasIsaVersion	= (lnLength_isa > 0);
				io->flags.hasOsVersion	= (lnLength_os  > 0);


			//////////
			// Data blocks
			//////
				if (uid_data_block != 0)
				{
					io->flags.hasDataBlockList = true;
					io->uid_data_block			= uid_data_block;
				}

			
			//////////
			// Code blocks
			//////
				if (uid_code_block != 0)
				{
					io->flags.hasCodeBlockList	= true;
					io->uid_code_block			= uid_code_block;
				}


			//////////
			// ISA Info
			//////
				if (isa)
				{
					// Copy into the SUbcIsaOs struct
					if (lnLength_isa > 0)
						memcpy(io + 1, isa, lnLength_isa);

					// Release memory
					free(isa);
				}


			//////////
			// OS Info
			//////
				if (os)
				{
					// Copy after the ISA if present, or at its position if ISA is not present
					if (lnLength_os > 0)
						memcpy((s8*)(io + 1) + lnLength_isa, os, lnLength_os);

					// Release memory
					free (os);
				}

		}

		// Signify
		return io;
	}




//////////
//
// Calculates the size of an SUbcText
//
//////
	s32 iiUbc_calcSbcTextSize(w16* text, SUbcText** textOut, SUbcError** error)
	{
		s32			lnLength2, lnLength;
		SUbcText*	t;


		// If it's empty, we don't store anything
		if (text == NULL)
			return 0;

		// Create the output param if specified
		lnLength = (s32)wcslen(text) * (s32)sizeof(w16);
		if (lnLength > 0xffff)
		{
			// The error
			if (error)
				*error = iiUbc_addError(_UBC_ERROR_STRING_TOO_LONG, iiUbc_getFirstNnChars(text, 32));

			// Signify the error
			return _UBC_ERROR_STRING_TOO_LONG;
		}

		lnLength2 = sizeof(t->length) + lnLength;
		if (textOut)
		{
			// Allocate content
			if ((t = (SUbcText*)calloc(1, lnLength2)))
			{
				// Store the length
				t->length = (u16)lnLength;
				memcpy(t->text, text, lnLength);
			}
		}

		// Signify the length
		return lnLength2;
	}




//////////
//
// Called to create an SUbcError entry
//
//////
	SUbcError* iiUbc_addError(u32 errorCode, w16* message)
	{
		SUbcError*	err;


		// Allocate an error
		err = (SUbcError*)calloc(1, sizeof(SUbcError));
		if (err)
		{
			// Populate
			err->errorCode	= errorCode;
			err->message	= message;
		}

		// Signify
		return err;
	}




//////////
//
// Called to copy the first Nn bytes
//
//////
	w16* iiUbc_getFirstNnChars(w16* text, s32 length)
	{
		w16*	copy;


		// Allocate a copy
		copy = (w16*)calloc(1, length * sizeof(w16));
		if (copy)
			wcscpy_s(copy, length, text);

		// Signify
		return copy;
	}
