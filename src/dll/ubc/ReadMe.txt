========================================================================
    DYNAMIC LINK LIBRARY : UBC Project Overview
========================================================================

UBC is the cross-platform / cross-OS Unity Binary Compilation ABI for

ubc.vcproj
    This project is used to create ubc.dll, which is the main DLL that
    handles all create, update, delete, and load/launch abilities.

ubc.cpp
    Main DLL source file.

const.h
	Constant definition

structs.h
	Structure definitions

defs.h
	Function prototype definitions

globals.h
	Global variable declarations

