//////////
//
// const.h
// Constant definitions
//
//////
//
// UBC -- Unity Binary Composition
// Apr.19.2020
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
//
#pragma once




#ifdef WIN32
	typedef wchar_t				w16;

	typedef char				s8;
	typedef short				s16;
	typedef long				s32;
	typedef __int64				s64;

	typedef unsigned char		u8;
	typedef unsigned short		u16;
	typedef unsigned long		u32;
	typedef unsigned __int64	u64;

	typedef float				f32;
	typedef double				f64;

#else
	typedef uint16_t			w16;

	typedef int8_t				s8;
	typedef int16_t				s16;
	typedef int32_t				s32;
	typedef int64_t				s64;

	typedef uint8_t				u8;
	typedef uint16_t			u16;
	typedef uint32_t			u32;
	typedef uint64_t			u64;

	typedef float				f32;
	typedef double				f64;
#endif




//////////
// Common error numbers
//////
	#define _ERROR_INVALID_PARAMETERS		-1
	#define _ERROR_FILE_DOES_NOT_EXIST		-2




//////////
// SUbcLink discriminating constants
//////
	#define _UBC_LINK_TYPE_STATS			0
	#define _UBC_LINK_TYPE_ISA_OS			1
	#define _UBC_LINK_TYPE_BUILD_DATE		2




//////////
// SUbcError constants
//////
	#define _UBC_ERROR_STRING_TOO_LONG		-1




//////////
// Signature magic numbers
//////
	#define _SIG1_MAGIC_NUMBER				0x1223334444555556
	#define _SIG2_MAGIC_NUMBER				0x7777777888888889




//////////
// _TYPE_* constants
//////
	#define _TYPE_VERSION					0x0001
	#define _TYPE_BUILD_DATE				0x0002
	#define _TYPE_TARGET_ISA_OS				0x0003
	#define _TYPE_DATA						0x0004
	#define _TYPE_CODE						0x0005
	#define _TYPE_DEBUG						0x0006
	#define _TYPE_REFERENCES				0x0007
	#define _TYPE_CONDITIONALS				0x0008
	#define _TYPE_SOURCE_FILE				0x0009
	#define _TYPE_PRELOADS					0x000a
	#define _TYPE_POSTLOADS					0x000b




//////////
// _ISA_* constants
// Supported ISAs
//////
	#define _ISA_ARM						0x00000001
	#define _ISA_X86						0x00000002
	#define _ISA_X64						0x00000003
	#define _ISA_RISC_V						0x00000004
	#define _ISA_DNET_IL					0x00000005




//////////
// _OS_* constants
// Supported operating systems
//////
	#define _OS_RAW	    					0x00000001
	#define _OS_WINDOWS 					0x00000002
	#define _OS_LINUX   					0x00000003
	#define _OS_CUSTOM						0x00000004




//////////
// Flags for Read, Write, Read/Write, Execute
//
// 		Low bit flags:  000
// 						|||
// 						||+---- Read
// 						|+----- Write
// 						+------ Execute
//////
	#define _READ							0x01
	#define _WRITE							0x02
	#define _EXECUTE						0x04




//////////
// _DATA_* constants
// Data block definitions
//////
	#define _DATA_READ_ONLY					0x1001
	#define _DATA_WRITE_ONLY				0x1002
	#define _DATA_READ_WRITE				0x1003
	#define _DATA_EXECUTE_ONLY				0x1004
	#define _DATA_EXECUTE_READ				0x1005
	#define _DATA_EXECUTE_WRITE				0x1006
	#define _DATA_EXECUTE_READ_WRITE		0x1007




//////////
// _CODE_* constants
// Code block definitions
//////
	#define _CODE_EXECUTE_ONLY				0x2004
	#define _CODE_EXECUTE_READ				0x2005
	#define _CODE_EXECUTE_WRITE				0x2006
	#define _CODE_EXECUTE_READ_WRITE		0x2007
