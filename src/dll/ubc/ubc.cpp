//////////
//
// ubc.cpp
//
//////
//
// Unity Binary Composition
// A cross-platform / cross-OS ABI for linked libraries and executables.
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
// Refer to:  https://www.codeproject.com/Articles/993067/Calling-Java-from-Cplusplus-with-JNI
// Refer to:  https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html
// Ability to create a JVM in a C++ app, and then run code in it, including calling its Java
// functions from C++.
//
//////
//
// Refer to ubc_jni.h for the JNI API.
// Refer to ubc_public.h for the public API.
//
//////
//
//




#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <io.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys\stat.h>

// JNI definition
#include <win32\jni_md.h>
#include "../../ubc_test/jni/com_rick_ubc_test_Main.h"

#include "const.h"
#include "structs.h"
#include "defs.h"
#include "globals.h"

// JNI code
#include "ubc_jni.h"

// Public API code (called by launcher.exe)
#include "ubc_public.h"

// Support functions
#include "ubc_misc.h"




//////////
//
// DLL main entry point
//
//////
	BOOL APIENTRY DllMain( HMODULE	hModule,
						   DWORD	ul_reason_for_call,
						   LPVOID	lpReserved	)
	{
		switch (ul_reason_for_call)
		{
			case DLL_PROCESS_ATTACH:
				break;

			case DLL_THREAD_ATTACH:
				break;

			case DLL_THREAD_DETACH:
				break;

			case DLL_PROCESS_DETACH:
				break;
		}
		return TRUE;
	}
