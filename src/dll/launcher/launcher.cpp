//////////
//
// launcher.cpp
//
//////
//
// Copyright (c) 2020 by Rick C. Hodgin
// Released under the PBL (Public Benefit License) v1.0
// See:  http://www.libsf.org/licenses/
//
// An unlimited license is hereby granted by Rick C. Hodgin to Peter Cheung
// and Quantr as part of a joint Unity Binary Compilation development project,
// relating to all materials, intellectual property, content, graphics, and
// everything else related to this project in its digital form.
//
// The base design and concepts contained herein are based on prior research
// done for ES/1 and ES/2, two custom operating systems being developed by Rick
// C. Hodgin as part of his ongoing Village Freedom Project:
//
//			In God's sight, we've come together.
//			We've come together to help each other.
//			Let's grow this project up ... together!
//			In service and Love to The Lord, forever!
//
//////
//
//


#include "stdafx.h"
#include "launcher.h"

#ifdef _DEBUG
	#include <io.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <fcntl.h>
	#include <sys\stat.h>
#endif

#include "../ubc/ubc.h"




//////////
//
// Main entry point
//
//////
	int APIENTRY wWinMain(	HINSTANCE	hInstance,
							HINSTANCE	hPrevInstance,
							LPWSTR		lpCmdLine,
							int			nCmdShow)
	{

		//////////
		// Added to allow time for the debugger to connect if running a Debug compile, and the file debug.txt exists in the current folder
		//////
			#ifdef _DEBUG
			{
				int		fileno, seconds;
				char	debug_txt[16];
				_sopen_s(&fileno, "debug.txt", _O_BINARY | _O_RDONLY, _SH_DENYNO, _S_IREAD);
				if (fileno >= 0)
				{
					// Load in the disk file content, which should begin with something like "60 seconds"
					_read(fileno, debug_txt, sizeof(debug_txt));
					_close(fileno);

					// Determine the number of seconds to pause, using a minimum of 20
					seconds = atoi(debug_txt);
					seconds = ((seconds <= 0) ? 20 : seconds);

					// Iterate on second at a time, so the user can set a breakpoint on the Sleep() line and immediately pause mid-delay
					for (int i = 0; i < seconds; ++i)
						Sleep(1000);	// Set breakpoint here
				}
			}
			#endif


		//////////
		// Launch the embedded executable
		//////
			return ubc_launch(hInstance, hPrevInstance, lpCmdLine, nCmdShow);

	}
